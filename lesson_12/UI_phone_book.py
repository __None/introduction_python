from tkinter import *
from tkinter import messagebox
import csv


class Contact:
    def __init__(self, name, number):
        self.name = name
        self.number = number


class PhoneBook:
    __file_path = "Contact_file.csv"
    __list_of_contact = []
    updated_contact = None

    def check_number(self, number):
        for i in self.__list_of_contact:
            if i.number == number:

                return True
        return False

    def add_contact(self, name, number):
        contact = Contact(name, number)
        self.__list_of_contact.append(contact)

    def __get_contact(self, some_name):
        for i in self.__list_of_contact:
            if i.name == some_name:

                return i

    def delete_contact(self, name_obj):
        self.__list_of_contact.remove(self.__get_contact(name_obj))

    def edit_contact(self, name_obj):
        self.updated_contact = self.__get_contact(name_obj)

        return self.updated_contact

    def save_contact(self, name, number):
        temp = self.__get_contact(self.updated_contact.name)
        temp.name = name
        temp.number = number
        self.updated_contact = None

    def read_contact(self):
        self.__list_of_contact = []
        with open(self.__file_path, "r", newline="") as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for i in csv_reader:
                contact = Contact(i[0], i[1])
                self.__list_of_contact.append(contact)

        return self.__list_of_contact

    def write_contact(self):
        with open(self.__file_path, mode='w', newline="") as contacts_file:
            contact_write = csv.writer(contacts_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            for i in self.__list_of_contact:
                contact_write.writerow([i.name, i.number])


some_contact = PhoneBook()


def delete():
    name_entry.delete(0, END)
    number_entry.delete(0, END)
    selection = languages_listbox.curselection()
    if selection:
        some_contact.delete_contact(languages_listbox.get(selection))
        languages_listbox.delete(selection)
    else:
        messagebox.showinfo("Ошибка", "Вы не выбрали запись для удаления")


def add():
    number = number_entry.get()
    name = name_entry.get()
    if name and number:
        if some_contact.check_number(number) is False:
            languages_listbox.insert(0, name)
            some_contact.add_contact(name, number)
        else:
            messagebox.showinfo("Ошибка", "Контакт с таким номером уже существует")
        name_entry.delete(0, END)
        number_entry.delete(0, END)
    else:
        messagebox.showinfo("Ошибка", "Вы не ввели данные")


def edit():
    name_entry.delete(0, END)
    number_entry.delete(0, END)
    selection = languages_listbox.curselection()
    if selection:
        contact = some_contact.edit_contact(languages_listbox.get(selection))
        name_entry.insert(0, contact.name)
        number_entry.insert(0, contact.number)
    else:
        messagebox.showinfo("Ошибка", "Вы не выбрали запись для редактирования")


def save():
    number = number_entry.get()
    name = name_entry.get()
    if name and number:
        for i in range(0, languages_listbox.size()):
            if languages_listbox.get(i) == some_contact.updated_contact.name:
                languages_listbox.delete(i)
                languages_listbox.insert(0, name)
        some_contact.save_contact(name, number)
        messagebox.showinfo("Сохранено", "Контакт сохранен")
        name_entry.delete(0, END)
        number_entry.delete(0, END)
    else:
        messagebox.showinfo("Ошибка", "Вы не ввели данные")


def read():
    languages_listbox.delete(0, languages_listbox.size())
    temp = some_contact.read_contact()
    for i in temp:
        languages_listbox.insert(0, i.name)
    messagebox.showinfo("Файл", "Контакты успешно считаны")


def write():
    some_contact.write_contact()
    messagebox.showinfo("Файл", "Контакты успешно перезаписаны")


root = Tk()
root.title("Телефонная книга")
root.geometry("290x600")

contact_name = Label(text="Имя:", fg="#000")
contact_name.place(x=20, y=20)

name_entry = Entry(width=40)
name_entry.place(x=20, y=45)

phone_number = Label(text="Номер телефона:", fg="#000")
phone_number.place(x=20, y=90)

number_entry = Entry(width=40)
number_entry.place(x=20, y=115)

add_button = Button(text="Добавить", command=add)
add_button.place(x=20, y=160)

edit_button = Button(text="Редактировать", command=edit)
edit_button.place(x=20, y=200)

remove_button = Button(text="Удалить", command=delete)
remove_button.place(x=210, y=160)

save_button = Button(text="Сохранить", command=save)
save_button.place(x=195, y=200)

read_button = Button(text="Чтение", command=read)
read_button.place(x=20, y=555)

write_button = Button(text="Запись", command=write)
write_button.place(x=215, y=555)

languages_listbox = Listbox(width=41, height=18)
languages_listbox.place(x=20, y=250)

root.mainloop()

# Jack Daniel,0887776655
# William Grant,0998887766
# Rick Smith,0776665544
# Jim Beam,0665554433
