some_list = open("list.txt", "r")


def some_splitter(my_list):
    right_list = []

    for i in my_list:
        right_side = i.split(" - ")
        right_list.append(right_side[1])

    number_list = []

    for i in right_list:
        temp = i.split(" ")

        for j in temp:
            if j.isdigit():
                number_list.append(int(j))

    return number_list


def first_code(number_list):
    code = 0

    for i in number_list:
        if i % 5 == 0:
            code += i

    return code


def decimal_to_binary(decimal_code):
    binary_code = ""

    while decimal_code > 0:
        binary_code = str(decimal_code % 2) + binary_code
        decimal_code = decimal_code // 2

    return binary_code


def most_often(strange_number):
    number_of_number = []

    for i in strange_number:
        temp_sum = 0
        for j in strange_number:
            if i == j:
                temp_sum += 1

        number_of_number.append(temp_sum)

    max_number = number_of_number[0]
    position = 0

    for i in range(len(number_of_number)):
        if max_number < number_of_number[i]:
            max_number = number_of_number[i]
            position = i

    return strange_number[position]


def defuse_the_bomb(binary_number):
    num0 = 0
    num1 = 0

    for i in str(binary_number):
        if i == "0":
            num0 += 1
        else:
            num1 += 1

    return num0 + num1


divided_list = some_splitter(some_list)
code_var = first_code(divided_list)
print("A bomb in the building - %i" % code_var)
binary_code_var = decimal_to_binary(code_var)
print("Open the door - %s" % binary_code_var)

strange_list = [2, 3, 5, 4, 2, 3, 5, 4, 3, 2, 2, 7, 8, 2, 2, 4, 1, 0, 2, 4]

most_often_num = most_often(strange_list)
most_often_num *= 7 * 2019
binary_often_num = decimal_to_binary(most_often_num)
secret_code = defuse_the_bomb(binary_often_num)
print("Input secret code - %i" % secret_code)
print("Yippee! Defuse!")
