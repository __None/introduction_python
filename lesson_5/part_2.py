def move_line_right(line):
    i = 0

    while i < len(line):
        j = 0
        while j < len(line) - 1:
            if line[j + 1] == 0 and line[j] != 0:
                line[j] = line[j] ^ line[j + 1]
                line[j + 1] = line[j] ^ line[j + 1]
                line[j] = line[j] ^ line[j + 1]
            j += 1
        i += 1
    print(line)


def move_line_left(line):
    i = 0

    while i < len(line):
        j = 0
        while j < len(line) - 1:
            if line[j] == 0 and line[j + 1] != 0:
                line[j] = line[j] ^ line[j + 1]
                line[j + 1] = line[j] ^ line[j + 1]
                line[j] = line[j] ^ line[j + 1]
            j += 1
        i += 1
    print(line)


def move_left(matrix):
    for row in matrix:
        move_line_left(row)


def move_right(matrix):
    for row in matrix:
        move_line_right(row)


my_matrix = [[2, 0, 1, 0, 1, 2, 2, 0, 4, 0, 4, 1],
             [0, 0, 1, 0, 1, 0, 2, 0, 4, 4, 4, 0],
             [0, 4, 0, 0, 1, 2, 2, 0, 4, 0, 4, 2],
             [0, 0, 1, 0, 1, 2, 0, 0, 4, 4, 4, 0]]
print(my_matrix)
move_left(my_matrix)
print()
move_right(my_matrix)

