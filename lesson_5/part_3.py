def show_matrix(matrix):
    for row in matrix:
        print(row)


def move_line_right(line):
    i = 0

    while i < len(line):
        j = 0
        while j < len(line) - 1:
            if line[j + 1] == 0 and line[j] != 0:
                line[j] = line[j] ^ line[j + 1]
                line[j + 1] = line[j] ^ line[j + 1]
                line[j] = line[j] ^ line[j + 1]
            j += 1
        i += 1
    print(line)


def move_line_left(line):
    i = 0

    while i < len(line):
        j = 0
        while j < len(line) - 1:
            if line[j] == 0 and line[j + 1] != 0:
                line[j] = line[j] ^ line[j + 1]
                line[j + 1] = line[j] ^ line[j + 1]
                line[j] = line[j] ^ line[j + 1]
            j += 1
        i += 1
    print(line)


def move_left(matrix):
    for row in matrix:
        move_line_left(row)


def move_right(matrix):
    for row in matrix:
        move_line_right(row)


def move_down(line):
    for h in range(len(line)):
        i = 0
        while i < len(line):
            j = 0
            while j < len(line) - 1:
                if line[j + 1][h] == 0 and line[j][h] != 0:
                    line[j][h] = line[j][h] ^ line[j + 1][h]
                    line[j + 1][h] = line[j][h] ^ line[j + 1][h]
                    line[j][h] = line[j][h] ^ line[j + 1][h]
                j += 1
            i += 1
    show_matrix(line)


def move_up(line):
    for h in range(len(line)):
        i = 0
        while i < len(line):
            j = 0
            while j < len(line) - 1:
                if line[j][h] == 0 and line[j + 1][h] != 0:
                    line[j][h] = line[j][h] ^ line[j + 1][h]
                    line[j + 1][h] = line[j][h] ^ line[j + 1][h]
                    line[j][h] = line[j][h] ^ line[j + 1][h]
                j += 1
            i += 1
    show_matrix(line)


my_matrix = [
    [1, 0, 0, 1, 0, 0, 0, 1, 0],
    [0, 0, 4, 1, 0, 0, 1, 1, 0],
    [0, 0, 4, 1, 2, 2, 1, 4, 4],
    [1, 1, 0, 0, 4, 2, 0, 4, 4],
    [2, 1, 2, 0, 4, 2, 2, 2, 2],
    [2, 2, 2, 0, 4, 0, 2, 2, 2],
    [2, 2, 0, 2, 0, 0, 0, 4, 4],
    [4, 2, 1, 2, 1, 2, 4, 4, 0],
    [4, 4, 1, 2, 1, 2, 4, 2, 0]
]
show_matrix(my_matrix)
# move_left(my_matrix)
print()
# move_right(my_matrix)
# print()
move_down(my_matrix)
print()
move_up(my_matrix)


