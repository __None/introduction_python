def move_right(line):
    i = 0

    while i < len(line):
        j = 0
        while j < len(line) - 1:
            if line[j + 1] == 0 and line[j] != 0:
                line[j] = line[j] ^ line[j + 1]
                line[j + 1] = line[j] ^ line[j + 1]
                line[j] = line[j] ^ line[j + 1]
            j += 1
        i += 1

    print(line)


def move_left(line):
    i = 0

    while i < len(line):
        j = 0
        while j < len(line) - 1:
            if line[j] == 0 and line[j + 1] != 0:
                line[j] = line[j] ^ line[j + 1]
                line[j + 1] = line[j] ^ line[j + 1]
                line[j] = line[j] ^ line[j + 1]
            j += 1
        i += 1
    print(line)


matrix = [0, 0, 1, 0, 1, 2, 2, 0, 4, 4, 4, 0]
print(matrix)
move_left(matrix)
move_right(matrix)
