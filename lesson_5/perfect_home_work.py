def show_matrix(not_matrix):
    for row in not_matrix:
        print(row)


def can_u_swap(variable_a, variable_b):
    variable_a = variable_a ^ variable_b
    variable_b = variable_a ^ variable_b
    variable_a = variable_a ^ variable_b

    return variable_a, variable_b


def can_u_sum(variable_a, variable_b):
    variable_a += variable_b
    variable_b = 0

    return variable_a, variable_b


def move_left(source_arr):
    for row in range(len(source_arr)):
        i = 0
        while i < len(source_arr):
            j = 0
            while j < len(source_arr) - i:
                temp_line = source_arr[row][j]
                temp_line1 = source_arr[row][j + 1]
                if temp_line == 0 and temp_line1 != 0:
                    source_arr[row][j], source_arr[row][j + 1] = can_u_swap(temp_line, temp_line1)
                j += 1
            i += 1

    return source_arr


def sum_left(method_matrix):
    left_matrix = move_left(method_matrix)
    for row in range(len(left_matrix)):
        for j in range(len(left_matrix) - 1):
            temp_left = left_matrix[row][j]
            temp_left1 = left_matrix[row][j + 1]
            if temp_left == temp_left1 and temp_left != 0:
                left_matrix[row][j], left_matrix[row][j + 1] = can_u_sum(temp_left, temp_left1)
    left_matrix = move_left(left_matrix)

    return left_matrix


def move_right(source_arr):
    for row in range(len(source_arr)):
        i = len(source_arr)
        while i > 0:
            j = len(source_arr) - 1
            while j > 0:
                temp_line = source_arr[row][j]
                temp_line0 = source_arr[row][j - 1]
                if temp_line == 0 and temp_line0 != 0:
                    source_arr[row][j], source_arr[row][j - 1] = can_u_swap(temp_line, temp_line0)
                j -= 1
            i -= 1

    return source_arr


def sum_right(method_matrix):
    right_matrix = move_right(method_matrix)
    for row in range(len(right_matrix)):
        for j in range(len(right_matrix) - 1, 0, -1):
            temp_right = right_matrix[row][j]
            temp_right0 = right_matrix[row][j - 1]
            if temp_right == temp_right0 and temp_right != 0:
                right_matrix[row][j], right_matrix[row][j - 1] = can_u_sum(temp_right, temp_right0)
    right_matrix = move_right(right_matrix)

    return right_matrix


def move_up(source_arr):
    for line in range(len(source_arr)):
        i = 0
        while i < len(source_arr):
            j = 0
            while j < len(source_arr) - 1:
                temp_row = source_arr[j][line]
                temp_row1 = source_arr[j + 1][line]
                if temp_row == 0 and temp_row1 != 0:
                    source_arr[j][line], source_arr[j + 1][line] = can_u_swap(temp_row, temp_row1)
                j += 1
            i += 1

    return source_arr


def sum_up(method_matrix):
    up_matrix = move_up(method_matrix)
    for line in range(len(up_matrix)):
        for j in range(len(up_matrix) - 1):
            temp_up = up_matrix[j][line]
            temp_up1 = up_matrix[j + 1][line]
            if temp_up == temp_up1 and temp_up != 0:
                up_matrix[j][line], up_matrix[j + 1][line] = can_u_sum(temp_up, temp_up1)
    up_matrix = move_up(up_matrix)

    return up_matrix


def move_down(source_arr):
    for line in range(len(source_arr)):
        i = len(source_arr)
        while i > 0:
            j = len(source_arr) - 1
            while j > 0:
                temp_row = source_arr[j][line]
                temp_row0 = source_arr[j - 1][line]
                if temp_row == 0 and temp_row0 != 0:
                    source_arr[j][line], source_arr[j - 1][line] = can_u_swap(temp_row, temp_row0)
                j -= 1
            i -= 1

    return source_arr


def sum_down(method_matrix):
    down_matrix = move_down(method_matrix)
    for line in range(len(down_matrix)):
        for j in range(len(down_matrix) - 1, 0, -1):
            temp_down = down_matrix[j][line]
            temp_down0 = down_matrix[j - 1][line]
            if temp_down == temp_down0 and temp_down != 0:
                down_matrix[j][line], down_matrix[j - 1][line] = can_u_sum(temp_down, temp_down0)
    down_matrix = move_down(down_matrix)

    return down_matrix


def game():
    matrix = [
        [1, 0, 0, 1, 0, 0, 0, 1, 0],
        [0, 0, 4, 1, 0, 0, 1, 1, 0],
        [0, 0, 4, 1, 2, 2, 1, 4, 4],
        [1, 1, 0, 0, 4, 2, 0, 4, 4],
        [2, 1, 2, 0, 4, 2, 2, 2, 2],
        [2, 2, 2, 0, 4, 0, 2, 2, 2],
        [2, 2, 0, 2, 0, 0, 0, 4, 4],
        [4, 2, 1, 2, 1, 2, 4, 4, 0],
        [4, 4, 1, 2, 1, 2, 4, 2, 0]
    ]
    show_matrix(matrix)
    print()

    while True:
        game_var = input("Choose moves ('left', 'right', 'up', 'down', else - exit): ")
        print()
        if game_var == "left":
            matrix = sum_left(matrix)
            show_matrix(matrix)
            print()
        elif game_var == "right":
            matrix = sum_right(matrix)
            show_matrix(matrix)
            print()
        elif game_var == "up":
            matrix = sum_up(matrix)
            show_matrix(matrix)
            print()
        elif game_var == "down":
            matrix = sum_down(matrix)
            show_matrix(matrix)
            print()
        else:
            print("Game over!")
            print()
            show_matrix(matrix)
            break


game()
