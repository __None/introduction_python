import random


def init_matrix(matrix, min_n, max_n):
    for i in range(len(matrix)):
        for j in range(len(matrix[0])):
            matrix[i][j] = random.randint(min_n, max_n)


def show_matrix(matrix):
    i = 0
    while i < len(matrix):
        j = 0
        row = matrix[i]

        while j < len(row):
            print(matrix[i][j], end=" ")
            j += 1

        print()
        i += 1


def show_different_matrix(matrix):
    i = 0
    while i < len(matrix):

        j = len()
        row = matrix[i]

        while j < i:

            print(matrix[i][j], end=" ")
            j += 1

        print()
        i += 1


n = 10
a = [[0] * n for i in range(n)]

init_matrix(a, 0, 9)

show_matrix(a)
print()
show_different_matrix(a)
