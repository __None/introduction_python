import random


def init_matrix(matrix, min_n, max_n):
    for i in range(len(matrix)):
        for j in range(len(matrix[0])):
            matrix[i][j] = random.randint(min_n, max_n)


def show_matrix(matrix):
    i = 0
    while i < len(matrix):
        j = 0
        row = matrix[i]

        while j < len(row):
            print(matrix[i][j], end=" ")
            j += 1

        print()
        i += 1


# def for_sum_line(matrix):
#     sum_var = []
#     for row in range(len(matrix)):
#         sum_row = 0
#         for i in matrix[row]:
#             sum_row += i  # Why this is work???
#         sum_var.append(sum_row)
#     print(sum_var)
#     return sum_var


def for_sum_line(matrix):  # Using a for loop
    sum_var = []
    for row in range(len(matrix)):
        sum_row = 0
        for i in range(len(matrix[row])):
            sum_row += matrix[row][i]
        sum_var.append(sum_row)
    print(sum_var)
    return sum_var


def while_sum_line(matrix):  # Using a while loop
    sum_var = []
    row = 0
    while row < len(matrix):
        sum_row = 0
        i = 0
        while i < len(matrix[row]):
            sum_row += matrix[row][i]
            i += 1
        sum_var.append(sum_row)
        row += 1
    print(sum_var)
    return sum_var


def for_move_matrix(matrix, sum_var):  # Using a for loop
    for i in sum_var:
        for j in range(len(sum_var) - 1):
            if sum_var[j] > sum_var[j + 1]:

                temp = sum_var[j]
                sum_var[j] = sum_var[j + 1]
                sum_var[j + 1] = temp

                temp = matrix[j]
                matrix[j] = matrix[j + 1]
                matrix[j + 1] = temp

    print(sum_var)
    print()
    show_matrix(matrix)


def while_move_matrix(matrix, sum_var):  # Using a while loop
    i = 0
    while i < len(sum_var):
        j = 0
        while j < len(sum_var) - 1:
            if sum_var[j] > sum_var[j + 1]:

                temp = sum_var[j]
                sum_var[j] = sum_var[j + 1]
                sum_var[j + 1] = temp

                temp = matrix[j]
                matrix[j] = matrix[j + 1]
                matrix[j + 1] = temp
            j += 1
        i += 1

    print(sum_var)
    print()
    show_matrix(matrix)


n = 10
my_matrix = [[0] * n for i in range(n)]

init_matrix(my_matrix, 0, 9)

show_matrix(my_matrix)
print()
sum_of_row = for_sum_line(my_matrix)
# sum_of_row = while_sum_line(my_matrix)
print()
for_move_matrix(my_matrix, sum_of_row)
# while_move_matrix(my_matrix, sum_of_row)
