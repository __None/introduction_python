import math


class PythonTest:

    def __init__(self, first_arr, second_arr):
        self.first_arr = first_arr
        self.second_arr = second_arr

    def total_with_for(self):
        total_with_for = 0
        for i in self.first_arr:
            total_with_for += i

        return total_with_for

    def total_with_while(self):
        total_with_while = 0
        i = 0
        while i < len(self.first_arr):
            total_with_while += self.first_arr[i]
            i += 1

        return total_with_while

    def combine_two_arrays(self):
        combine_two_arrays = []
        i = 0
        if len(self.first_arr) > len(self.second_arr):
            temp = len(self.first_arr)
        else:
            temp = len(self.second_arr)
        while i < temp:
            if i < len(self.first_arr):
                combine_two_arrays.append(self.first_arr[i])
            if i < len(self.second_arr):
                combine_two_arrays.append(self.second_arr[i])
            i += 1

        return combine_two_arrays

    @staticmethod
    def get_fibonacci_numbers(n):
        fibonacci_numbers = [0, 1]
        i = 1
        while i < n - 1:
            fibonacci_numbers.append(fibonacci_numbers[i] + fibonacci_numbers[i - 1])
            i += 1

        return fibonacci_numbers

    def biggest_number_from_list(self, arr):
        for i in range(len(arr) - 1):
            for j in range(len(arr) - i - 1):
                first_element = self.__first_n_digits(arr[j], 1)
                second_element = self.__first_n_digits(arr[j + 1], 1)
                if first_element < second_element:
                    temp = arr[j]
                    arr[j] = arr[j + 1]
                    arr[j + 1] = temp

        return arr

    @staticmethod
    def __first_n_digits(number, n):
        return number // 10 ** (int(math.log(number, 10)) - n + 1)


arr1 = [50, 2, 1, 9]
arr2 = ['a', 'b', 'c']

p = PythonTest(arr1, arr2)

task_1 = p.total_with_for()
task_2 = p.total_with_while()
task_3 = p.combine_two_arrays()
task_4 = p.get_fibonacci_numbers(15)
task_5 = p.biggest_number_from_list(arr1)

print("total_with_for: %i" % task_1)
print("total_with_while: %i" % task_2)
print("combine_two_arrays: ", task_3)
print("fibonacci_numbers:", task_4)
print("biggest_number_from_list:", task_5)
