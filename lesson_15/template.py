import math


class PythonTest:

    def __init__(self, first_arr, second_arr):
        self.first_arr = first_arr
        self.second_arr = second_arr

    def total_with_for(self):
        pass

    def total_with_while(self):
        pass

    def combine_two_arrays(self):
        pass

    @staticmethod
    def get_fibonacci_numbers(n):
        pass

    def biggest_number_from_list(self, arr):
        pass

    @staticmethod
    def __first_n_digits(number, n):
        return number // 10 ** (int(math.log(number, 10)) - n + 1)


arr1 = [50, 2, 1, 9]
arr2 = ['a', 'b', 'c']

p = PythonTest(arr1, arr2)


