def show_matrix(matrix):
    for row in matrix:
        for x in row:
            print(x, end=" ")
        print()
    


m = [[1, 2, 3], [2, 3, 4], [3, 4, 5], [4, 5, 6]]

show_matrix(m)
