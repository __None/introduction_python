import random


n = 12

field = [[0] * n for i in range(n)]


def init_bomb(matrix, bomb_min, bomb_max):
    for i in range(len(matrix)-2):
        for j in range(len(matrix[0])-2):
            matrix[i+1][j+1] = random.randint(bomb_min, bomb_max)


def show_matrix(matrix):
    for row in matrix:
        for x in row:
            print(x, end=" ")
        print()


def number_bombs(matrix):
    num_all_bombs = 0
    for row in matrix:
        for x in row:
            if x == 1:
                num_all_bombs += 1
    return num_all_bombs


def bombs_around(y, x):
    bomb_number = 0
    for i in range(x - 1, x + 2):
        for j in range(y - 1, y + 2):
            if field[j][i] == 1:
                bomb_number += 1
    return bomb_number


# def bombs_around_v2(y, x):
#     bomb_number = 0
#     if x == 0:
#     for i in range(x - 1, x + 2):
#         for j in range(y - 1, y + 2):
#             if field[j][i] == 1:
#                 bomb_number += 1
#     return bomb_number


def input_data():
    # print("Select the coordinate of the cell you want to open.")
    while True:
        inputted_row = int(input("Input row in range from 1 to 10: "))
        inputted_column = int(input("Input column in range from 1 to 10: "))
        if 0 < inputted_row <= 10 and 0 < inputted_column <= 10:
            break
        # else:
        #     print("Incorrect data entered!\nPlease, try again...")
    return inputted_row, inputted_column


# def defuse():
#     print("Select the coordinate of the cell you want to defuse.")
#     while True:
#         inputted_row = int(input("Input row in range from 1 to 10: "))
#         inputted_column = int(input("Input column in range from 1 to 10: "))
#         if 0 < inputted_row <= 10 and 0 < inputted_column <= 10:
#             field[inputted_row][inputted_column] = 7
#             break
#         else:
#             print("Incorrect data entered!\nPlease, try again...")
#     return True


# def game_mod():
#     print("Selected action defuse(7)/open(8)")
#     while True:
#         action = int(input())
#         if action == 8:
#             input_data()
#             break
#         elif action == 7:
#             defuse()
#         else:
#             print("Incorrect data entered!\nPlease, try again...")


init_bomb(field, 0, 1)

show_matrix(field)

# all_bomb = number_bombs(field)
#
# print("Number of all bombs: %i" % all_bomb)
#
#
# my_row, my_column = input_data()
#
# res = bombs_around(my_row, my_column)
#
# print("Number of bombs: %i" % res)

while True:
    all_bomb = number_bombs(field)

    print("Bombs: %i" % all_bomb)

    my_row, my_column = input_data()

    if field[my_row][my_column] == 1:
        print("You lose =(")
        break
    else:
        res = bombs_around(my_row, my_column)
        print("Bombs around: %i" % res)
