import random


def init_matrix(matrix, min_n, max_n):
    for i in range(len(matrix)):
        for j in range(len(matrix[0])):
            matrix[i][j] = random.randint(min_n, max_n)


def show_matrix(matrix):
    for row in matrix:
        for x in row:
            print(x, end=" ")
        print()


n = 10
a = [[0] * n for i in range(n)]

init_matrix(a, 1, 101)

show_matrix(a)
