import random


def init_matrix(matrix, min_n, max_n):
    for i in range(len(matrix)):
        for j in range(len(matrix[0])):
            matrix[i][j] = random.randint(min_n, max_n)


def show_matrix(matrix):
    for row in matrix:
        for x in row:
            print(x, end=" ")
        print()


def min_max(matrix):
    my_max = matrix[0][0]
    my_min = matrix[0][0]
    sum_even = 0
    sum_NOTeven = 0
    my_even_arr = []
    my_ueven_arr = []
    for row in matrix:
        for x in row:
            if x > my_max:
                my_max = x

            if x < my_min:
                my_min = x

            if x % 2 == 0:
                my_even_arr.append(x)
                sum_even += x
            else:
                my_ueven_arr.append(x)
                sum_NOTeven += x
    print("Max: %d" % my_max, " ", end="Min: %d" % my_min)
    print()
    print("Sum Even: %d" % sum_even, " ", end="Sum Ueven: %d" % sum_NOTeven)
    print()
    print(my_even_arr)
    print()
    print(my_ueven_arr)


n = 10
a = [[0] * n for i in range(n)]

init_matrix(a, 1, 101)

show_matrix(a)
min_max(a)
