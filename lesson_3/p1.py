import random


def init_matrix(matrix, min_n, max_n):
    for i in range(len(matrix)):
        for j in range(len(matrix[0])):
            matrix[i][j] = random.randint(min_n, max_n)


def show_matrix(matrix):
    for row in matrix:
        for x in row:
            print(x, end=" ")
        print()


def row_number(row):
    my_max = row[0]
    my_min = row[0]
    for x in row:
        if x > my_max:
            my_max = x

        if x < my_min:
            my_min = x
    print("Max: %d" % my_max, " ", end="Min: %d" % my_min)
    print()


def show_row(matrix):
    for row in matrix:
        row_number(row)


n = 10
a = [[0] * n for i in range(n)]

init_matrix(a, 1, 101)

show_matrix(a)
show_row(a)
