class Person:
    # age = 18
    # name = "John"

    # age = 0
    # name = ""

    def __init__(self, n, a):
        self.name = n
        self.age = a

    def show_personal_data(self):
        print("Name: %s, Age: %d" % (self.name, self.age))


p1 = Person("Mike", 20)
p1.show_personal_data()

p2 = Person("Liza", 22)
p2.show_personal_data()

# p2 = Person()
# p2.name = "Liza"
# p2.age = 22
# p2.show_personal_data()
