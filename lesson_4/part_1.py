class MyClass:
    variable = "blah"

    def my_function(self):
        print("This is a message inside the class.")


my_object = MyClass()

print(my_object.variable)
my_object.my_function()
