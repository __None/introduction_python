import random


class Field:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.my_field = [[0] * x for i in range(y)]

    def init_bomb(self):
        number_of_bombs = (self.x * self.y) * 25 / 100
        print(number_of_bombs)
        n = 0
        while number_of_bombs != n:
            b_x = random.randint(0, self.x - 1)
            b_y = random.randint(0, self.y - 1)
            if self.my_field[b_x][b_y] != 1:
                self.my_field[b_x][b_y] = 1
                n += 1

    def print_field(self):
        for row in self.my_field:
            for x in row:
                print(x, end=" ")
            print()


field = Field(10, 10)
field.init_bomb()
field.print_field()
