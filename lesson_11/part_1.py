# class Book - name, author, year, genre, pages, ID
# class Library - list of books
# methods: find book by name, find books by author, find book by genre, find book by yaer
# sort book by genre, sort book by year
# add book, delete book, update book
# save to file
# reade from file


class Book:
    def __init__(self, number, name, author, year, genre, pages):
        self.number = number
        self.name = name
        self.author = author
        self.year = year
        self.genre = genre
        self.pages = pages


class Library:
    __list_of_books = []
    __counter = 0

    def add_book(self, name, author, year, genre, pages):
        book = Book(self.__counter, name, author, year, genre, pages)

        is_book_exists = self.__get_book(name)

        if not is_book_exists:
            self.__list_of_books.append(book)
            self.__counter += 1
        else:
            print("Book with such name already exists!")

    def find_book_by_name(self, name):
        for book in self.__list_of_books:
            if book.name == name:
                return book
        return None

    def show_books(self):
        for book in self.__list_of_books:
            print(book.name + " - " + book.author + " - " + book.year)

    def remove_book(self, book):
        self.__list_of_books.remove(book)

    def remove_book_by_name(self, name):
        for book in self.__list_of_books:
            if book.name == name:
                self.__list_of_books.remove(book)
        return None

    def __get_book(self, name):
        for book in self.__list_of_books:
            if book.name == name:
                return book
        return None


some_book = Library()
some_book.add_book("451 градус по Фаренгейту", "Рэй Дуглас Брэдбери", "1953", "Научная фантастика, антиутопия, философский роман", "288")
some_book.add_book("Гарри Поттер", "Джоан Роулинг", "1997", "Роман, Фэнтези", "455")
some_book.show_books()

q = some_book.find_book_by_name("Гарри Поттер")
print(q.author)

# some_book.remove_book(q)
# some_book.remove_book_by_name("451 градус по Фаренгейту")
# some_book.show_books()
