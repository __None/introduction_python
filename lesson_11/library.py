# class Book - id, name, author, year, genre, pages
# class Library - list of books
# methods: find book by name, find books by author, find book by genre, find book by year
# sort book by genre, sort book by year
# add book, delete book, update book
# save to file
# read from file
import uuid


class Book:
    def __init__(self, name, author, year, genre, pages):
        self.__number = uuid.uuid4()
        self.name = name
        self.author = author
        self.year = year
        self.genre = genre
        self.pages = pages

    def get_number(self):
        return self.__number


class Library:
    __list_of_books = []

    def add_book(self, name, author, year, genre, pages):
        book = Book(name, author, year, genre, pages)

        is_book_exists = self.__get_book(name)

        if is_book_exists is None:
            self.__list_of_books.append(book)
        else:
            print("Book with such name already exists!")

    def find_book_by_name(self, name):
        return self.__get_book(name)

    def remove_book(self, book):
        self.__list_of_books.remove(book)

    def remove_book_by_name(self, name):
        book = self.__get_book(name)
        self.__list_of_books.remove(book)

    def update_book(self, book):
        for n in range(len(self.__list_of_books)):
            if self.__list_of_books[n].get_number() == book.get_number():
                self.__list_of_books[n] = book
                break

    def sort_by_name(self):
        self.__list_of_books.sort(key=lambda x: x.name, reverse=False)

    def show_books(self):
        for book in self.__list_of_books:
            print(book.name + " - " + book.author + " - " + book.year)

    def remove_book_by_author(self, author):

        new_list = [i for i in self.__list_of_books if i.author != author]
        self.__list_of_books = new_list
        print("Deleted!")

        # for book in self.__list_of_books:
        #     if book.author == author:
        #         self.__list_of_books.remove(book)
        # print("Deleted!")

    def __get_book(self, name):
        for book in self.__list_of_books:
            if book.name == name:
                return book

        return None


some_book = Library()
some_book.add_book("451 градус по Фаренгейту", "Рэй Брэдбери", "1953", "Научная фантастика, антиутопия", "288")
some_book.add_book("Гарри Поттер", "Джоан Роулинг", "1997", "Роман, Фэнтези", "455")
some_book.add_book("Фаренгейт", "Рэй Дуглас Брэдбери", "1953", "Научная фантастика, антиутопия", "288")
some_book.add_book("Не_Гарри Поттер 4", "Джоан Роулинг", "2004", "Роман, Фэнтези", "666")
some_book.add_book("Гарри Поттер 2", "Джоан Роулинг", "2004", "Роман, Фэнтези", "666")
some_book.add_book("Гарри Поттер 3", "Джоан Роулинг", "2004", "Роман, Фэнтези", "589")
some_book.add_book("Гарри Поттер 7", "Джоан Роулинг", "2004", "Роман, Фэнтези", "899")

some_book.show_books()

# book = some_book.find_book_by_name("Гарри Поттер")
# book.name = "Ice and Fire"
# some_book.update_book(book)

# q = some_book.find_book_by_name("Гарри Поттер")
# print(q.author)
# some_book.sort_by_name()
some_book.remove_book_by_author("Джоан Роулинг")

some_book.show_books()
