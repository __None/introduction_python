class Node:
    def __init__(self, value, next_node):
        self.value = value
        self.next_node = next_node


class LinkedList:
    head = None

    def __init__(self):
        pass

    def add(self, value):
        current = self.head
        if current is None:
            self.head = Node(value, None)
        else:
            while current.next_node is not None:
                current = current.next_node

            current.next_node = Node(value, None)

    def show_all(self):
        while self.head is not None:
            print(self.head.value)
            self.head = self.head.next_node

    def push(self, value):  # myself
        temp = self.head
        self.head = Node(value, temp)

    def insert(self, value, index):
        current = self.head
        counter = 0
        while current is not None:
            if counter == index:
                temp = current.next_node
                current.next_node = Node(value, temp)
                break
            elif current.next_node is None:
                current.next_node = Node(value, None)
                break
            counter += 1
            current = current.next_node


linked_list = LinkedList()
linked_list.add(1)
linked_list.add(2)
linked_list.add(3)
linked_list.add(4)

linked_list.push(0)

linked_list.insert(5, 3)
linked_list.insert(9, 12)
linked_list.insert(18, 0)

linked_list.show_all()
