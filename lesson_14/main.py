class Node:
    def __init__(self, value, next_node):
        self.value = value
        self.next_node = next_node


class LinkedList:
    head: Node = None

    def add(self, value):
        current = self.head
        if current is None:
            self.head = Node(value, None)
        else:
            while current.next_node is not None:
                current = current.next_node

            current.next_node = Node(value, None)

    def push(self, value):
        old_head = self.head
        self.head = Node(value, old_head)

    def show_all(self):
        while self.head is not None:
            print(self.head.value)
            self.head = self.head.next_node


linked_list = LinkedList()
linked_list.add(1)
linked_list.add(2)
linked_list.add(3)


linked_list.show_all()
