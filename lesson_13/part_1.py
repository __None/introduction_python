from random import randrange
from tkinter import *


class Formula:
    __symbols = ["+", "-", "*", "/"]

    def __init__(self, speed, x, y):
        self.formula = self.__generate_formula()
        self.correct_answer = str(eval(self.formula))
        self.speed = speed
        self.x = x
        self.y = y

    def __generate_formula(self):

        # i = randrange(3)
        # symbol = self.symbols[i]

        symbol = "+"

        a = randrange(100)
        b = randrange(100)

        formula = str(a) + symbol + str(b)

        return formula


class Game:
    def __init__(self, number_of_lives, number_of_formulas):
        self.number_of_lives = number_of_lives
        self.number_of_formulas = number_of_formulas
        self.formulas = []
        self.__create_list_of_formulas()

    def __create_list_of_formulas(self):
        for i in range(self.number_of_formulas):
            some_formula = Formula(0, randrange(25, 355, 55), randrange(50, 410, 40))
            self.formulas.append(some_formula)

    def answer(self, user_input):
        for i in self.formulas:
            if i.correct_answer == user_input:
                new_formula = Formula(0, i.x, i.y + 10)
                self.formulas.remove(i)
                self.formulas.append(new_formula)
                draw_label()
                # TODO: Remove formula from list
                # TODO: Generate new formula

    def next_iteration(self):
        # TODO: In case formula reached the bottom you need to start game again
        pass


g = Game(3, 10)

label_list = []

root = Tk()

root.title("Game001")
root.geometry("400x600")

canvas = Canvas()
canvas.create_line(25, 510, 375, 510)
canvas.pack(fill=BOTH, expand=1)

entry_answer = Entry()
entry_answer.place(x=130, y=520, in_=root)


def draw_label():

    for i in g.formulas:
        formula_label = Label(text=i.formula, fg="#000")
        label_list.append(formula_label)
        formula_label.place(x=i.x, y=i.y)


draw_label()


def pressed_ENTER(event):
    g.answer(entry_answer.get())
    entry_answer.delete(0, END)

    # formula_label.destroy()


root.bind("<Return>", pressed_ENTER)

root.mainloop()

# print(f.formula + "=" + str(f.correct_answer))
