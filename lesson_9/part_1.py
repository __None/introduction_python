import csv
import uuid


class Contact:
    def __init__(self, number, first_name, last_name, phone_number, address):
        self.number = number
        self.first_name = first_name
        self.last_name = last_name
        self.phone_number = phone_number
        self.address = address
        self.__secret_code = uuid.uuid4()

    def what_a_code(self):
        return self.__secret_code


class ListOfContacts:
    __file_path = "List_of_contacts.csv"
    __listOfContacts = []
    __counter = 1

    def add_contact(self, first_name, last_name, phone_number, address):

        for i in self.__listOfContacts:
            if i.phone_number == phone_number:
                return

        contact = Contact(self.__counter, first_name, last_name, phone_number, address)

        self.__listOfContacts.append(contact)
        self.__counter += 1

    def update_contact(self, some_obj):  # Home work method
        for i in range(len(self.__listOfContacts)):
            if self.__listOfContacts[i].what_a_code() == some_obj.what_a_code():
                self.__listOfContacts[i] = some_obj

    def show_all_contacts(self):
        for i in self.__listOfContacts:
            print("Number: %i" % i.number)
            print("First name: %s" % i.first_name)
            print("Last name: %s" % i.last_name)
            print("Phone number: %s" % i.phone_number)
            print("Address: %s" % i.address)
            print()

    def find_contact(self, name):
        if name != "":
            for i in self.__listOfContacts:
                if i.first_name == name or i.last_name == name:
                    return i

        print("None contact find")
        return None

    def remove_contact(self, contact):
        self.__listOfContacts.remove(contact)
        print("Contact removed!")
        print()

    def save_to_file(self):
        with open(self.__file_path, mode='w') as contacts_file:
            contact_write = csv.writer(contacts_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

            for i in self.__listOfContacts:
                contact_write.writerow([i.number, i.first_name, i.last_name, i.phone_number, i.address])

    def __read_from_file(self):
        with open(self.__file_path) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                contact = Contact(row[0], row[1], row[2], row[3], row[4])
                self.__listOfContacts.append(contact)
                line_count += 1


some_contact = ListOfContacts()

some_contact.add_contact("Jack", "Daniel", "0887776655", "Green Street, 29")
some_contact.add_contact("Jim", "Beam", "0665554433", "Yellow Street, 31")
some_contact.add_contact("William", "Grant", "0998887766", "Black Wood Street, 13")

some_contact.show_all_contacts()

found_contact = some_contact.find_contact("Jim")

found_contact.first_name = "Rick"
found_contact.last_name = "Smith"
found_contact.phone_number = "0776665544"

some_contact.update_contact(found_contact)

some_contact.show_all_contacts()

some_contact.save_to_file()
