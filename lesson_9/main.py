import csv


class Contact:
    def __init__(self, number, first_name, last_name, phone_number,
                 address):
        self.number = number
        self.first_name = first_name
        self.last_name = last_name
        self.phone_number = phone_number
        self.address = address


class ListOfContacts:
    __file_path = "list_of_contacts.csv"
    __listOfContacts = []
    __counter = 1

    def __init__(self):
        self.__read_from_file()

    def add_contact(self, first_name, last_name, phone_number, address):

        if any(c for c in self.__listOfContacts if c.phone_number == phone_number):
            return

        contact = Contact(self.__counter, first_name, last_name, phone_number, address)
        self.__listOfContacts.append(contact)
        self.__counter += 1

    def find_contact(self, name):
        if name != "":
            for c in self.__listOfContacts:
                if c.first_name == name or c.last_name == name:
                    return c

        return None

    def remove_contact(self, contact):
        self.__listOfContacts.remove(contact)

    def update_contact(self, contact):
        pass

    def show_all_contacts(self):
        for c in self.__listOfContacts:
            print(c.number)
            print("Name: ", c.first_name, c.last_name)
            print("Phone number: ", c.phone_number)
            print("Address: ", c.address)
            print()

    def save_to_file(self):
        with open(self.__file_path, mode='w') as contacts_file:
            contact_writer = csv.writer(contacts_file,
                                        delimiter=',',
                                        quotechar='"',
                                        quoting=csv.QUOTE_MINIMAL)

            for c in self.__listOfContacts:
                contact_writer.writerow([c.number, c.first_name,
                                         c.last_name, c.phone_number, c.address])

    def __read_from_file(self):
        with open(self.__file_path) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                contact = Contact(row[0], row[1], row[2], row[3], row[4])
                self.__listOfContacts.append(contact)
                line_count += 1


contacts = ListOfContacts()

contacts.show_all_contacts()

# c = contacts.find_contact("Brown")
# c.first_name = "sdfsdf"
# c.last_name = "sdfsdf"
# c.phone_number = "dfgdfg"

# contacts.update_contact(c)

# contacts.save_to_file()
#
# contacts.show_all_contacts()
#
# c = contacts.find_contact("Brown")
# contacts.remove_contact(c)

