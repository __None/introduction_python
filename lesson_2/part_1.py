def my_function():
    print("Hello World")


def hello(name):
    print("Hello %s" % name)


def my_sum(a, b):
    return a + b


def Calc(name, a, b):
    hello(name)
    return my_sum(a, b)


# my_function()
# hello("John")

# result = my_sum(10, 5)
result = Calc("John", 10, 5)

print(result)

