def show(a):
    for i in range(a):  # i = 0 -> a - 1
        print(i)


def show(a, b):
    for i in range(a, b):  # i = a -> b - 1
        print(i)


def show_while(a):
    i = 0

    while i <= a:
        if i == 5:
            i += 1
            continue

        if i == 450:
            break

        print(i)
        i += 1


# show(10)
# show(11, 20)
show_while(10)
