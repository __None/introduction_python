# Найти самое длинное слово. В строке состоящей из слов разделенных пробелом.

my_str = "python java c c++ javascript pascal php kavascript"


def word_long(words):
    splitted_str = words.split(" ")
    word = 0
    long = 0
    for i in splitted_str:
        if long < len(i):
            long = len(i)
            word = i
        elif long == len(i):
            word += ", " + i
    return long, word


res_long, res_word = word_long(my_str)

print("The longest word(s): %s. Word length: %i." % (res_word, res_long))
