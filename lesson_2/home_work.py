# Какая версия лучше?


def is_palindrome(word):

    number = len(word)

    # for i in range(number):  # Home work v2.0
    #     number -= 1
    #     if word[i] != word[number]:
    #         return False
    # return True

    # for i in range(number):  # Home work v1
    #     number -= 1
    #     if word[i] == word[number]:
    #         print("Looks good!")
    #     else:
    #         print("Oops!")
    #         return False
    # return True

    my_range = int(number / 2)

    for i in range(my_range):  # Home work v2.1
        number -= 1
        if word[i] != word[number]:
            return False
    return True


my_word = input("Enter your word: ")

result = is_palindrome(my_word)

if result:
    print("Word is palindrome!")
else:
    print("Word if not palindrome!")
