def my_sum(a, b):
    return a + b


def my_dif(a, b):
    return a - b


def my_mul(a, b):
    return a * b


def my_div(a, b):
    return a / b


def calculator(formula):  # formula = "a + b" 0 < a < 9, 0 < b < 9 (+, -, /, *)
    a = int(formula[0])
    b = int(formula[2])
    s = formula[1]

    result = 0

    if s == "+":
        result = my_sum(a, b)

    elif s == "-":
        result = my_dif(a, b)

    elif s == "*":
        result = my_mul(a, b)

    else:
        if b == 0:
            print("WARNING!")
        else:
            result = my_div(a, b)
    return result


data = "985 * 5"

splitted = data.split(" ")

result = calculator(splitted)

print(result)  # 25

# Возврат длины слова
# s = "Hello"
# s1 = len(s)
