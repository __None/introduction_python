contacts = {}  # contacts = {"John": 1234568, "Adam": 7658921}

contacts["John"] = 1234568
contacts["John"] = 1681531535
contacts["Adam"] = 7658921
contacts["Kenny"] = 7651688

print(contacts["John"])

for name, number in contacts.items():
    print("Phone number of %s is %d" % (name, number))


