class Animal:
    def __init__(self, name, weight, age, breed):
        self.name = name
        self.weight = weight
        self.age = age
        self.breed = breed

    def say(self):
        print("I am talking")

    def print_info(self):
        print("Name: % s, " % self.name, "Weight: % .3f, " % self. weight, "Age:% i, " % self.age, "Breed: % s, " % self.breed)


class Cat(Animal):
    def say(self):
        print("Meow")


class Dog(Animal):
    def say(self):
        print("Woof")


class Raccoon(Animal):
    def say(self):
        Animal.say(self)
        print("I am a Raccoon and I am cool ;)")


dachshund = Dog("Lucifer", 6.6, 6, "Dachshund")
hoome_raccoon = Raccoon("Sparki", 10, 5, "Hoome")
skogkatt = Cat("Grieg", 3.4, 2, "Skogkatt")
wild_raccoon = Raccoon("Fang", 8.8, 6, "Wild")
geralt = Cat("Geralt", 6.5, 4, "Maine coon")

Animals = [dachshund, hoome_raccoon, skogkatt, wild_raccoon, geralt]

for i in Animals:
    i.print_info()
    i.say()

################### Класс с домашки #######################################

# class Triangle(Shape):
#     def __init__(self, h):
#         self.h = h
#
#     def __square(self):
#         return 0.5 * self.h * self.h
#
#     def draw(self, symbol, color):
#         f = 1
#         for i in range(self.h):
#             for j in range(f):
#                 print(colored(symbol, color), end=" ")
#             f += 1
#             print()
#         print("Square: % .2f" % self.__square())

# Реализация Олега

#         f = 1
#         while f < self.h:
#             print(colored(symbol * f, color))
#             f += 1
