from termcolor import colored
import colorama
colorama.init()


class Shape:

    def _square(self):
        pass

    def draw(self, symbol, color):
        pass


class Rect(Shape):
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def _square(self):
        return self.a * self.b

    def draw(self, symbol, color):
        for i in range(self.a):
            for j in range(self.b):
                print(colored(symbol, color), end=" ")
            print()
        print("Square: %.2f" % self._square())


class Triangle(Shape):
    def __init__(self, h):
        self.h = h

    def _square(self):
        return 0.5 * self.h * self.h  # При нашем условии рисования он всё равно получится равнобедренным

    def draw(self, symbol, color):
        f = 1
        for i in range(self.h):
            for j in range(f):
                print(colored(symbol, color), end=" ")
            f += 1
            print()
        print("Square: %.2f" % self._square())


some_triangle = Rect(4, 6)
some_triangle.draw("0", "red")  # grey, red, green, yellow, blue, magenta, cyan, white

some_triangle = Triangle(4)
some_triangle.draw("*", "yellow")

# while True:
#     choice = int(input("Choice Rect(1), Triangle(2) or Exit(else): "))
#     if choice == 1:
#         a = int(input("Input a > 0: "))
#         b = int(input("Input b > 0: "))
#         in_symbol = input("Input symbol: ")
#         print("Choice color: grey, red, green, yellow, blue, magenta, cyan, white")
#         in_color = input("Input color: ")
#
#         some_rect = Rect(a, b)
#         some_rect.draw(in_symbol, in_color)
#     elif choice == 2:
#         h = int(input("Input h > 0: "))
#         in_symbol = input("Input symbol: ")
#         print("Choice color: grey, red, green, yellow, blue, magenta, cyan, white")
#         in_color = input("Input color: ")
#
#         some_triangle = Triangle(h)
#         some_triangle.draw(in_symbol, in_color)
#     else:
#         break
