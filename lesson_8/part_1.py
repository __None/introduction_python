class Person:
    def __init__(self, name, age, address, phone_number):
        self.name = name
        self.age = age
        self.address = address
        self.phone_number = phone_number

    def print_info(self):
        print(self.name, self.age, self.address, self.phone_number)

    def _protected(self):
        print("I am a protected method")


class Teacher(Person):
    def __init__(self, name, age, address, phone_number, class_id):
        Person.__init__(self, name, age, address, phone_number)
        self.class_id = class_id

    def print_info(self):
        print(self.name, self.age, self.address, self.phone_number, self.class_id)
        self.__private()
        self._protected()

    def __private(self):
        print("I am a private method")


class Student(Person):
    def init(self):
        pass


class Cleaner(Person):
    def init(self):
        pass


t = Teacher("John", 32, "Green Street", "0665436527", "1A")
s = Student("James", 19, "Yellow Street", "0996543828")
c = Cleaner("Ann", 43, "Red Street", "0998885544")

t.name = "Jim"

t.print_info()
s.print_info()
c.print_info()




