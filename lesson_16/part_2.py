class Product:
    def __init__(self, name, prise, number):
        self.name = name
        self.prise = prise
        self.number = number


class Shop:
    __list_of_product = []

    def add_product(self, name, prise, number):
        some_product = Product(name, prise, number)
        self.__list_of_product.append(some_product)

    def show_all_product(self):
        for i in self.__list_of_product:
            print("Name: %s; Prise: %s; Number: %s" % (i.name, i.prise, i.number))

    def name_find_product_(self, name):
        for i in self.__list_of_product:
            if i.name == name:
                return i

    def delete_product(self, prod):
        pass


shop = Shop()

shop.add_product("Tea", "15.45", "45")
shop.add_product("Beer", "24.50", "50")

shop.show_all_product()
