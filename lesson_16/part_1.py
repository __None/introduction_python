class Product:
    def __init__(self, name, prise, number):
        self.name = name
        self.prise = prise
        self.number = number


class Book(Product):
    def __init__(self, name, prise, number, author, pages):
        Product.__init__(self, name, prise, number)
        self.author = author
        self.pages = pages


class Pen(Product):
    def __init__(self, name, prise, number, color, pen_type):
        Product.__init__(self, name, prise, number)
        self.color = color
        self.pen_type = pen_type


class Shop:
    __list_of_product = []

    def add_book(self, name, prise, number, author, pages):
        some_book = Book(name, prise, number, author, pages)
        self.__list_of_product.append(some_book)

    def add_pen(self, name, prise, number, color, pen_type):
        some_pen = Pen(name, prise, number, color, pen_type)
        self.__list_of_product.append(some_pen)

    def add_product(self, name, prise, number):
        some_product = Product(name, prise, number)
        self.__list_of_product.append(some_product)

    def show_all_product(self):
        for i in self.__list_of_product:
            print("Name: %s; Prise: %.2f; Number: %i" % (i.name, i.prise, i.number))

    def name_find_product_(self, name):
        for i in self.__list_of_product:
            if i.name == name:
                return i

    def sort_by_prise(self, sort_type):
        for i in range(len(self.__list_of_product) - 1):
            for j in range(len(self.__list_of_product) - i - 1):
                first_obj_from_list = self.__list_of_product[j]
                second_obj_from_list = self.__list_of_product[j + 1]
                if sort_type:
                    if first_obj_from_list.prise < second_obj_from_list.prise:
                        temp = self.__list_of_product[j]
                        self.__list_of_product[j] = self.__list_of_product[j + 1]
                        self.__list_of_product[j + 1] = temp
                else:
                    if first_obj_from_list.prise > second_obj_from_list.prise:
                        temp = self.__list_of_product[j]
                        self.__list_of_product[j] = self.__list_of_product[j + 1]
                        self.__list_of_product[j + 1] = temp

    def sort_by_number(self, sort_type):
        for i in range(len(self.__list_of_product) - 1):
            for j in range(len(self.__list_of_product) - i - 1):
                first_obj_from_list = self.__list_of_product[j]
                second_obj_from_list = self.__list_of_product[j + 1]
                if sort_type:
                    if first_obj_from_list.number < second_obj_from_list.number:
                        temp = self.__list_of_product[j]
                        self.__list_of_product[j] = self.__list_of_product[j + 1]
                        self.__list_of_product[j + 1] = temp
                else:
                    if first_obj_from_list.number > second_obj_from_list.number:
                        temp = self.__list_of_product[j]
                        self.__list_of_product[j] = self.__list_of_product[j + 1]
                        self.__list_of_product[j + 1] = temp


shop = Shop()

shop.add_product("Tea", 15.45, 45)
shop.add_product("Beer", 100.50, 50)
shop.add_product("Candy", 78.50, 800)
shop.add_product("Fish", 259.00, 5)

shop.add_book("Гарри Поттер 7", 899.00, 2, "Джоан Роулинг", 655)

shop.add_pen("Red pen", 5.00, 100, "Red", "Ordinary")

shop.show_all_product()
print()

shop.sort_by_prise(0)

shop.show_all_product()
print()

shop.sort_by_number(1)

shop.show_all_product()
